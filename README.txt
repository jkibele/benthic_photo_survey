Benthic Photo Survey has Moved!!
================================

The new BPS web page is: http://jkibele.github.io/benthic_photo_survey/

The new BPS repository is: https://github.com/jkibele/benthic_photo_survey

The new BPS documentation is: http://benthic-photo-survey.readthedocs.org/en/latest/

The version in this bitbucket repository is out of date. Please do not use it.